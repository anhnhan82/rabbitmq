<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 8/15/17
 * Time: 13:27
 */

namespace Inside\Queues\Queue\Api;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductArrival implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $arrParams;

    /**
     * Create a new job instance.
     *
     * @param  Podcast  $podcast
     * @return void
     */
    public function __construct($arrParams)
    {
        $this->arrParams = $arrParams;
    }

    /**
     * Execute the job.
     *
     * @param  AudioProcessor  $processor
     * @return void
     */
    public function handle(){
        print_r($this->arrParams);
    }

}